# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Ayca Omrak <aycaom@hotmail.com>, 2013
# Bullgeschichte <bullgeschichte@riseup.net>, 2015
# cmldrs, 2014
# cmldrs, 2014
# imratirtil <d.imra.gundogdu@gmail.com>, 2014
# ecocan <eecocan@yahoo.com>, 2014
# ecocan <eecocan@yahoo.com>, 2014
# imratirtil <d.imra.gundogdu@gmail.com>, 2014
# Kaya Zeren <kayazeren@gmail.com>, 2015-2017
# metint, 2014
# metint, 2014
# Ozancan Karataş <ozancankaratas96@outlook.com>, 2015-2016
# Tails_developers <tails@boum.org>, 2014
# Tails_developers <tails@boum.org>, 2014
# T. E. Kalayci <tekrei@fsfe.org>, 2017
# Ümit Türk <zucchinitr@gmail.com>, 2013
# Volkan Gezer <volkangezer@gmail.com>, 2013-2016
# Yasin Özel <iletisim@yasinozel.com.tr>, 2013
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-03-12 19:03+0100\n"
"PO-Revision-Date: 2017-12-28 12:06+0000\n"
"Last-Translator: Kaya Zeren <kayazeren@gmail.com>\n"
"Language-Team: Turkish (http://www.transifex.com/otf/torproject/language/"
"tr/)\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:39
msgid "Tor is ready"
msgstr "Tor hazır"

#: config/chroot_local-includes/etc/NetworkManager/dispatcher.d/60-tor-ready.sh:40
msgid "You can now access the Internet."
msgstr "Artık İnternet'e erişebilirsiniz."

#: config/chroot_local-includes/etc/whisperback/config.py:65
#, python-format
msgid ""
"<h1>Help us fix your bug!</h1>\n"
"<p>Read <a href=\"%s\">our bug reporting instructions</a>.</p>\n"
"<p><strong>Do not include more personal information than\n"
"needed!</strong></p>\n"
"<h2>About giving us an email address</h2>\n"
"<p>\n"
"Giving us an email address allows us to contact you to clarify the problem. "
"This\n"
"is needed for the vast majority of the reports we receive as most reports\n"
"without any contact information are useless. On the other hand it also "
"provides\n"
"an opportunity for eavesdroppers, like your email or Internet provider, to\n"
"confirm that you are using Tails.\n"
"</p>\n"
msgstr ""
"<h1>Karşılaştığınız sorunu çözmemize yardım edin!</h1>\n"
"<p><a href=\"%s\">Hata bildirme yönergelerini</a> okuyun.</p>\n"
"<p><strong>Gerektiğinden fazla kişisel bilgi vermeyin!</strong></p>\n"
"<h2>E-posta adresinizi bildirmeniz hakkında</h2>\n"
"<p>\n"
"Bir e-posta adresi bildirmeniz, sorununuzu daha iyi anlamak için sizinle "
"iletişim\n"
"kurmamızı sağlar. Bize iletilen pek çok hata bildirimi, iletişim bilgileri "
"bulunmadığı\n"
"için işe yaramıyor. Ancak gerçek bilgilerinizi verdiğinizde sizi izliyor "
"olabilecek \n"
"kurumlara (e-posta ya da İnternet servis sağlayıcınıza) Tails yazılımını "
"kullandığınızı\n"
"belirleme fırsatı vermiş olursunuz. Bu nedenle farklı bir e-posta adresi "
"kullanmanız önerilir.\n"
"</p>\n"

#: config/chroot_local-includes/usr/local/bin/electrum:57
msgid "Persistence is disabled for Electrum"
msgstr "Electrum için kalıcılık devre dışı bırakıldı"

#: config/chroot_local-includes/usr/local/bin/electrum:59
msgid ""
"When you reboot Tails, all of Electrum's data will be lost, including your "
"Bitcoin wallet. It is strongly recommended to only run Electrum when its "
"persistence feature is activated."
msgstr ""
"Tails yazılımını yeniden başlattığınızda, Bitcoin cüzdanınız da dahil tüm "
"Electrum verileri kaybolur. Electrum yazılımının yalnız kalıcılık özelliği "
"etkin iken çalıştırılması önerilir."

#: config/chroot_local-includes/usr/local/bin/electrum:60
msgid "Do you want to start Electrum anyway?"
msgstr "Electrum yine de başlatılsın mı?"

#: config/chroot_local-includes/usr/local/bin/electrum:63
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:41
msgid "_Launch"
msgstr "_Başlat"

#: config/chroot_local-includes/usr/local/bin/electrum:64
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:42
msgid "_Exit"
msgstr "Çı_kış"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:75
msgid "Restart"
msgstr "Yeniden başlat"

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:78
msgid "Lock screen"
msgstr ""

#: config/chroot_local-includes/usr/share/gnome-shell/extensions/status-menu-helper@tails.boum.org/extension.js:81
msgid "Power Off"
msgstr "Gücü Kapat"

#: config/chroot_local-includes/usr/local/bin/tails-about:22
#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:1
msgid "Tails"
msgstr "Tails"

#: config/chroot_local-includes/usr/local/bin/tails-about:25
#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:1
msgid "About Tails"
msgstr "Tails Hakkında"

#: config/chroot_local-includes/usr/local/bin/tails-about:35
msgid "The Amnesic Incognito Live System"
msgstr "The Amnesic Incognito Live System"

#: config/chroot_local-includes/usr/local/bin/tails-about:36
#, python-format
msgid ""
"Build information:\n"
"%s"
msgstr ""
"Yapım bilgisi:\n"
"%s"

#: config/chroot_local-includes/usr/local/bin/tails-about:54
msgid "not available"
msgstr "kullanılamıyor"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:170
#, fuzzy
msgid "Your additional software installation failed"
msgstr "Ek yazılımlarınız"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:171
#, fuzzy
msgid ""
"The installation failed. Please check your additional software "
"configuration, or read the system log to understand better the problem."
msgstr ""
"Güncellenemedi. Bu durum bir ağ sorunundan kaynaklanıyor olabilir. Lütfen ağ "
"bağlantınızı denetleyip Tails yazılımını yeniden başlatmayı deneyin. Sorunu "
"daha iyi anlayabilmek için sistem günlüğünü okuyabilirsiniz."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:177
#, fuzzy
msgid "Your additional software are installed"
msgstr "Ek yazılımlarınız"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:178
#, fuzzy
msgid "Your additional software are ready to use."
msgstr "Ek yazılımlarınız"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:194
#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:204
#, fuzzy
msgid "Your additional software upgrade failed"
msgstr "Ek yazılımlarınız"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:195
#, fuzzy
msgid ""
"The check for upgrades failed. This might be due to a network problem. "
"Please check your network connection, try to restart Tails, or read the "
"system log to understand better the problem."
msgstr ""
"Güncellenemedi. Bu durum bir ağ sorunundan kaynaklanıyor olabilir. Lütfen ağ "
"bağlantınızı denetleyip Tails yazılımını yeniden başlatmayı deneyin. Sorunu "
"daha iyi anlayabilmek için sistem günlüğünü okuyabilirsiniz."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:201
#, fuzzy
msgid "Your additional software are up to date"
msgstr "Ek yazılımlarınız"

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:202
msgid "The upgrade was successful."
msgstr "Güncellendi."

#: config/chroot_local-includes/usr/local/sbin/tails-additional-software:205
msgid ""
"The upgrade failed. This might be due to a network problem. Please check "
"your network connection, try to restart Tails, or read the system log to "
"understand better the problem."
msgstr ""
"Güncellenemedi. Bu durum bir ağ sorunundan kaynaklanıyor olabilir. Lütfen ağ "
"bağlantınızı denetleyip Tails yazılımını yeniden başlatmayı deneyin. Sorunu "
"daha iyi anlayabilmek için sistem günlüğünü okuyabilirsiniz."

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:52
msgid "Synchronizing the system's clock"
msgstr "Sistem saati eşitleniyor "

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:53
msgid ""
"Tor needs an accurate clock to work properly, especially for Hidden "
"Services. Please wait..."
msgstr ""
"Tor yazılımının düzgün çalışabilmesi için saatin doğru olması önemlidir, "
"özellikle Gizli Servisler için. Lütfen bekleyin..."

#: config/chroot_local-includes/usr/local/lib/tails-htp-notify-user:87
msgid "Failed to synchronize the clock!"
msgstr "Saat eşitlenemedi!"

#: config/chroot_local-includes/usr/local/bin/tails-security-check:124
msgid "This version of Tails has known security issues:"
msgstr "Bu Tails sürümünde bilinen bazı güvenlik sorunları var: "

#: config/chroot_local-includes/usr/local/bin/tails-security-check:134
msgid "Known security issues"
msgstr "Bilinen güvenlik sorunları"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:52
#, sh-format
msgid "Network card ${nic} disabled"
msgstr "${nic} ağ kartı devre dışı"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:53
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}) so it is "
"temporarily disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"${nic_name} (${nic}) ağ kartı için MAC maskelemesi başarısız bu yüzden "
"geçici olarak devre dışı bırakıldı.\n"
"Tails'i yeniden başlatmanız ve MAC maskelemesini kapatmanız gerekebilir."

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:62
msgid "All networking disabled"
msgstr "Tüm ağ devre dışı"

#: config/chroot_local-includes/usr/local/lib/tails-spoof-mac:63
#, sh-format
msgid ""
"MAC spoofing failed for network card ${nic_name} (${nic}). The error "
"recovery also failed so all networking is disabled.\n"
"You might prefer to restart Tails and disable MAC spoofing."
msgstr ""
"${nic_name} (${nic}) ağ kartı için MAC maskelemesi başarısız. Ayrıca bu "
"hatanın kurtarılma girişimi de başarısız dolayısıyla tüm ağ devre dışı.\n"
"Tails uygulamasını yeniden başlatmanız ve MAC maskelemesini kapatmanız "
"gerekebilir."

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:109
msgid "Lock Screen"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:118
#: config/chroot_local-includes/usr/local/bin/tor-browser:46
msgid "Cancel"
msgstr "İptal"

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:124
msgid "Screen Locker"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:130
msgid "Set up a password to unlock the screen."
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:135
msgid "Password"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-screen-locker:141
msgid "Confirm"
msgstr ""

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:35
#, fuzzy
msgid ""
"\"<b>Not enough memory available to check for upgrades.</b>\n"
"\n"
"Make sure this system satisfies the requirements for running Tails.\n"
"See file:///usr/share/doc/tails/website/doc/about/requirements.en.html\n"
"\n"
"Try to restart Tails to check for upgrades again.\n"
"\n"
"Or do a manual upgrade.\n"
"See https://tails.boum.org/doc/first_steps/upgrade#manual\""
msgstr ""
"<b>Güncellemeleri denetlemek için yeterli bellek yok.</b>\n"
"\n"
"Bu sistemin Tails yazılımının gereksinimlerini karşıladığına emin olun.\n"
"Bilgiler: file:///usr/share/doc/tails/website/doc/about/requirements.tr."
"html\n"
"\n"
"Güncellemeleri denetlemek için Tails yazılımını yeniden başlatmayı deneyin.\n"
"\n"
"Ya da el ile güncelleyin.\n"
"Bilgiler: https://tails.boum.org/doc/first_steps/upgrade#manual"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:72
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:27
msgid "error:"
msgstr "hata:"

#: config/chroot_local-includes/usr/local/bin/tails-upgrade-frontend-wrapper:73
#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:28
msgid "Error"
msgstr "Hata"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:71
msgid "Warning: virtual machine detected!"
msgstr "Uyarı: Sanal makine algılandı!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:73
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails."
msgstr ""
"Hem ana işletim sistemi hem de sanallaştırma yazılımı Tails üzerinde neler "
"yaptığınızı görebilir."

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:76
msgid "Warning: non-free virtual machine detected!"
msgstr "Uyarı: Özgür olmayan sanal makine algılandı!"

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:78
msgid ""
"Both the host operating system and the virtualization software are able to "
"monitor what you are doing in Tails. Only free software can be considered "
"trustworthy, for both the host operating system and the virtualization "
"software."
msgstr ""
"Sunucu işletim sistemi ve sanallaştırma yazılımı Tails üzerinde ne "
"yaptığınızı izler. Sunucu işletim sistemi ve sanallaştırma yazılımı için "
"yalnızca özgür yazılımlar güvenilir olarak düşünülebilir."

#: config/chroot_local-includes/usr/local/lib/tails-virt-notify-user:83
msgid "Learn more"
msgstr "Daha fazla bilgi"

#: config/chroot_local-includes/usr/local/bin/tor-browser:43
msgid "Tor is not ready"
msgstr "Tor hazır değil"

#: config/chroot_local-includes/usr/local/bin/tor-browser:44
msgid "Tor is not ready. Start Tor Browser anyway?"
msgstr "Tor hazır değil. Gene de Tor Browser başlatılsın mı?"

#: config/chroot_local-includes/usr/local/bin/tor-browser:45
msgid "Start Tor Browser"
msgstr "Tor Browser'ı Başlat"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:38
msgid "Do you really want to launch the Unsafe Browser?"
msgstr "Güvenli Olmayan Tarayıcıyı başlatmak istediğinize emin misiniz?"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:40
msgid ""
"Network activity within the Unsafe Browser is <b>not anonymous</b>.\\nOnly "
"use the Unsafe Browser if necessary, for example\\nif you have to login or "
"register to activate your Internet connection."
msgstr ""
"Güvensiz Tarayıcı ile ağ etkinlikleriniz <b>anonim değildir</b>.\\nGüvensiz "
"Tarayıcıyı yalnızca kullanmanız gerekiyorsa, örneğin\\nİnternet bağlantınızı "
"etkinleştirmek için kaydolmanız ya da oturum açmanız gerekiyorsa kullanın."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:51
msgid "Starting the Unsafe Browser..."
msgstr "Güvenli Olmayan Tarayıcı başlatılıyor..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:52
msgid "This may take a while, so please be patient."
msgstr "Bu işlem biraz zaman alabilir, lütfen sabırlı olun."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:57
msgid "Shutting down the Unsafe Browser..."
msgstr "Güvenli Olmayan Tarayıcı kapatılıyor..."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:58
msgid ""
"This may take a while, and you may not restart the Unsafe Browser until it "
"is properly shut down."
msgstr ""
"Bu işlem biraz zaman alabilir. Güvenli Olmayan Tarayıcı düzgün kapatılmadan "
"yeniden başlatamayabilirsiniz."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:70
msgid "Failed to restart Tor."
msgstr "Tor yeniden başlatılamadı."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:84
#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:1
msgid "Unsafe Browser"
msgstr "Güvenli Olmayan Tarayıcı"

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:92
msgid ""
"Another Unsafe Browser is currently running, or being cleaned up. Please "
"retry in a while."
msgstr ""
"Şu anda başka bir Güvenli Olmayan Tarayıcı çalışıyor ya da temizleniyor. "
"Lütfen bir süre sonra yeniden deneyin."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:100
msgid "Failed to setup chroot."
msgstr "chroot kurulamadı."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:105
msgid "Failed to configure browser."
msgstr "Tarayıcıyı ayarlanamadı."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:111
msgid ""
"No DNS server was obtained through DHCP or manually configured in "
"NetworkManager."
msgstr ""
"DHCP aracılığıyla bir DNS sunucusu bulunamadı ya da AğYöneticisi içinde el "
"ile ayarlanmış."

#: config/chroot_local-includes/usr/local/sbin/unsafe-browser:117
msgid "Failed to run browser."
msgstr "Tarayıcıyı çalıştırılamadı."

#: ../config/chroot_local-includes/etc/skel/Desktop/Report_an_error.desktop.in.h:1
msgid "Report an error"
msgstr "Hata bildirin"

#: ../config/chroot_local-includes/etc/skel/Desktop/tails-documentation.desktop.in.h:1
#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:1
msgid "Tails documentation"
msgstr "Tails belgeleri"

#: ../config/chroot_local-includes/usr/share/applications/tails-documentation.desktop.in.h:2
msgid "Learn how to use Tails"
msgstr "Tails yazılımını nasıl kullanacağınızı öğrenin"

#: ../config/chroot_local-includes/usr/share/applications/tails-about.desktop.in.h:2
msgid "Learn more about Tails"
msgstr "Tails hakkında ayrıntılı bilgi alın"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:1
msgid "Tor Browser"
msgstr "Tor Browser"

#: ../config/chroot_local-includes/usr/share/applications/tor-browser.desktop.in.h:2
msgid "Anonymous Web Browser"
msgstr "Anonim Web Tarayıcısı"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:2
msgid "Browse the World Wide Web without anonymity"
msgstr "Web üzerinde anonim olmadan gez"

#: ../config/chroot_local-includes/usr/share/applications/unsafe-browser.desktop.in.h:3
msgid "Unsafe Web Browser"
msgstr "Güvenli Olmayan Tarayıcı"

#: ../config/chroot_local-includes/usr/share/desktop-directories/Tails.directory.in.h:2
msgid "Tails specific tools"
msgstr "Özel Tails araçları"

#: ../config/chroot_local-includes/usr/share/polkit-1/actions/org.boum.tails.root-terminal.policy.in.h:1
msgid "To start a Root Terminal, you need to authenticate."
msgstr "Bir Root Uç Birimi başlatmak için kimliğinizi doğrulamalısınız."

#~ msgid ""
#~ "NetworkManager passed us garbage data when trying to deduce the clearnet "
#~ "DNS server."
#~ msgstr ""
#~ "AğYöneticisi, Clearnet DNS sunucusunu anlamaya çalışırken anlamsız bir "
#~ "veri aktardı."
