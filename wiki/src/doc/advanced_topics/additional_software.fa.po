# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2016-09-02 17:26+0200\n"
"PO-Revision-Date: 2015-10-14 19:35+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/"
"additional_software/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Install additional software\"]]\n"
msgstr "[[!meta title=\"نصب نرم‌افزارهای اضافی\"]]\n"

#. type: Plain text
msgid ""
"Tails includes a [[coherent but limited set of applications|doc/about/"
"features]]. More applications can be installed as on any Debian system. Only "
"applications that are packaged for Debian can be installed. To know if an "
"application is packaged for Debian, and to find the name of the "
"corresponding software packages, you can search for it in the [[Debian "
"package directory|https://www.debian.org/distrib/packages]]."
msgstr ""
"تیلز حاوی [[مجموعه‌ای منسجم اما محدود از نرم‌افزارها|doc/about/features]] است. "
"روی تیلز مانند هر سیستم دبیان دیگری می‌توان نرم‌افزارهای دیگری نیز نصب کرد. "
"تنها می‌توان نرم‌افزارهایی نصب کرد برای دبیان بسته‌بندی شده باشند. برای فهمیدن "
"این که آیا یک نرم‌افزار برای دبیان بسته‌بندی شده یا نه و برای یافتن نام بستهٔ "
"نرم‌افزاری مرتبط می‌توانید در [[مجموعهٔ بسته‌های دبیان|https://www.debian.org/"
"distrib/packages]] دنبال آن بگردید."

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr ""

#. type: Title =
#, fuzzy, no-wrap
#| msgid "To install additional software packages:"
msgid "Installing additional software packages\n"
msgstr "برای نصب کردن بسته‌های نرم‌افزاری اضافی:"

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>The packages included in Tails are carefully tested for security.\n"
"Installing additional packages might break the security built in Tails.\n"
"Be careful with what you install.</p>\n"
msgstr ""
"<p>بسته‌های موجود در تیلز با دقت از نظر امنیتی بررسی شده‌اند.\n"
"نصب بسته‌های اضافی ممکن است امنیت موجود در تیلز را دچار مخاطره کند.\n"
"مراقب باشید که چه چیزی نصب می‌کنید.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid ""
"Since Tails is amnesic, any additional software package needs to be reinstalled in each working\n"
"session. To install the same software packages automatically at the beginning of every working session use the\n"
"[[<span class=\"guilabel\">Additional software packages</span> persistence feature|doc/first_steps/persistence/configure#additional_software]] instead.\n"
msgstr ""
"از آن‌جا که تیلز ابزار فراموشی است، هر بستهٔ نرم‌افزاری بیشتری باید در هر نشست کاری دوباره نصب شود.\n"
"برای نصب خودکار همان بسته‌های نرم‌افزاری در آغاز هر نشست کاری از [[ویژگی مانای <span class=\"guilabel\">بسته‌های نرم‌افزاری اضافی</span>  |doc/first_steps/persistence/configure#additional_software]] استفاده کنید.\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid "<p>Packages that use the network need to be configured to go through Tor. They are otherwise blocked from accessing the network.</p>\n"
msgstr "<p>بسته‌هایی که با اینترنت کار می‌کنند باید طوری تنظیم شوند که از طور استفاده کنند. در غیر این صورت از دسترسی آن‌ها به اینترنت جلوگیری می‌شود.</p>\n"

#. type: Plain text
msgid "To install additional software packages:"
msgstr "برای نصب کردن بسته‌های نرم‌افزاری اضافی:"

#. type: Bullet: '1. '
msgid ""
"[[Set up an administration password|doc/first_steps/startup_options/"
"administration_password]]."
msgstr ""
"[[یک گذرواژهٔ مدیریتی بسازید|doc/first_steps/startup_options/"
"administration_password]]."

#. type: Bullet: '2. '
msgid ""
"Open a [[root terminal|doc/first_steps/startup_options/"
"administration_password#open_root_terminal]]."
msgstr ""
"یک [[پایانهٔ اصلی|doc/first_steps/startup_options/"
"administration_password#open_root_terminal]] باز کنید."

#. type: Bullet: '3. '
msgid ""
"Execute the following command to update the lists of available packages:"
msgstr "این فرمان را برای به‌روزرسانی فهرست‌های بسته‌های موجود اجرا کنید:"

#. type: Plain text
#, no-wrap
msgid "       apt update\n"
msgstr "       apt update\n"

#. type: Bullet: '3. '
msgid ""
"To install an additional package, execute the following command, replacing "
"`[package]` with the name of the package that you want to install:"
msgstr ""
"برای نصب یک بستهٔ اضافی این خط فرمان را اجرا کرده و در آن `[package]` را با "
"نام بستهٔ مورد نظرتان جایگزین کنید:"

#. type: Plain text
#, no-wrap
msgid "       apt install [package]\n"
msgstr "       apt install [package]\n"

#. type: Plain text
#, no-wrap
msgid "   For example, to install the package `ikiwiki`, execute:\n"
msgstr "   برای نمونه برای نصب کردن بستهٔ `ikiwiki` این فرمان را اجرا کنید:\n"

#. type: Plain text
#, no-wrap
msgid "       apt install ikiwiki\n"
msgstr "       apt install ikiwiki\n"

#. type: Plain text
#, fuzzy, no-wrap
msgid "   <div class=\"note\">\n"
msgstr "   <div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <p>You can also write multiple package names to install several packages at the same\n"
"   time. If a package has dependencies, those will be installed\n"
"   automatically.</p>\n"
msgstr ""
"   <p>همچنین می‌توانید نام چندین بستهٔ را بنویسید تا چندین بسته را هم‌زمان نصب کنید\n"
"   اگر یک بسته زیرشاخه‌هایی دارد آن‌‌ها نیز به طور خودکار\n"
"   نصب می‌شوند.</p>\n"

#. type: Plain text
#, no-wrap
msgid "   </div>\n"
msgstr "   </div>\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"repository\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Configuring additional APT repositories\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "<p>The packages included in Tails are carefully tested for security.\n"
#| "Installing additional packages might break the security built in Tails.\n"
#| "Be careful with what you install.</p>\n"
msgid ""
"<p>The packages included in Tails are carefully tested for security.\n"
"Configuring additional APT repositories might break the security built in Tails.\n"
"Be careful with what you install.</p>\n"
msgstr ""
"<p>بسته‌های موجود در تیلز با دقت از نظر امنیتی بررسی شده‌اند.\n"
"نصب بسته‌های اضافی ممکن است امنیت موجود در تیلز را دچار مخاطره کند.\n"
"مراقب باشید که چه چیزی نصب می‌کنید.</p>\n"

#. type: Plain text
msgid ""
"Sometimes, you might need to configure additional APT repositories. For "
"example, to install packages from the `non-free` section of Debian. To do so:"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Create a <span class=\"filename\">apt-sources.list.d</span> folder in your "
"persistent volume:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "       sudo install -d -m 755 /live/persistence/TailsData_unlocked/apt-sources.list.d\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Edit <span class=\"filename\">/live/persistence/TailsData_unlocked/"
"persistence.conf</span>, the configuration of the persistent volume, as root "
"and add the <span class=\"filename\">apt-sources.list.d</span> folder as a "
"persistence feature of type `link`:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "       /etc/apt/sources.list.d\tsource=apt-sources.list.d,link\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Write your additional `sources.list` files in the <span class=\"filename"
"\">apt-sources.list.d</span> folder. For example, to add the `non-free` "
"sections of Debian Jessie, backports, and security updates, you can create a "
"file named <span class=\"filename\">/live/persistence/TailsData_unlocked/apt-"
"sources.list.d/non-free.list</span> with the following content:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"       deb tor+http://ftp.us.debian.org/debian/ jessie non-free\n"
"       deb tor+http://ftp.us.debian.org/debian/ jessie-backports non-free\n"
"       deb tor+http://security.debian.org/ jessie/updates non-free\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   File names must end with\n"
"   <span class=\"filename\">.list</span> and may only contain the following\n"
"   characters: letters, digits, underscore, hyphen, and period.\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Correct the ownership and permissions of your additional `sources.list` "
"files to be owned by `root` and only readable by others. For example:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"       chown root:root /live/persistence/TailsData_unlocked/apt-sources.list.d/non-free.list\n"
"       chmod 644 /live/persistence/TailsData_unlocked/apt-sources.list.d/non-free.list\n"
msgstr ""

#. type: Bullet: '1. '
msgid "Restart Tails to apply the changes."
msgstr ""
